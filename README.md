## Autores

* **Alexandre Silva dos Santos** 
* **Anibal de Medeiros Batista Filho** 
* **Ronaldo Costa Cordeiro** 
* **Rodrigo Oliveira Gomes dos Santos** 
* **Marcel Marques** [Github](https://github.com/marcmatias)
* **Taironne Darley Torres Matos** [Github](https://gitlab.com/taironne.matos)