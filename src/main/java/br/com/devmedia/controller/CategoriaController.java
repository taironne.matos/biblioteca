package br.com.devmedia.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.devmedia.dao.CategoriaDao;
import br.com.devmedia.domain.Categoria;

@Controller
@RequestMapping("categoria")
public class CategoriaController {
	
	@Autowired
	private CategoriaDao dao;
	
	@RequestMapping(value="/todos", method=RequestMethod.GET)
	public ModelAndView listarTodos(ModelMap model) {
		model.addAttribute("categorias", dao.getTodos());
		return new ModelAndView("category/list", model);
	}	
	
	@GetMapping("/cadastro")
	public ModelAndView cadastro(@ModelAttribute("categoria") Categoria categoria, ModelMap model) {
		return new ModelAndView("category/add", model);		
	}
	
	@PostMapping("/save")
	public String save(@ModelAttribute("categoria") Categoria categoria, RedirectAttributes attr) {
		dao.salvar(categoria);
		attr.addFlashAttribute("message", "Categoria cadastrada com sucesso.");
		return "redirect:/categoria/todos";
	}
	
	@GetMapping("/update/{id}")
	public ModelAndView preUpdate(@PathVariable("id") Long id, ModelMap model) {
		Categoria categoria = dao.getId(id);
		model.addAttribute("categoria", categoria);
		return new ModelAndView("/category/add", model);	
		
	}
	
	@PostMapping("/update")
	public String update(@ModelAttribute("categoria") Categoria categoria, RedirectAttributes attr) {
		dao.editar(categoria);		
		attr.addFlashAttribute("message", "Categoria alterada com suacesso.");
		return "redirect:/categoria/todos";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, RedirectAttributes attr) {
		dao.excluir(id);
		attr.addFlashAttribute("message", "Categoria deletada com suacesso.");
		return "redirect:/categoria/todos";
	}
	
}
