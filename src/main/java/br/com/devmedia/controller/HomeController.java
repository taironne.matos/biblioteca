package br.com.devmedia.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.devmedia.domain.Usuario;

@Controller
@RequestMapping("/")
public class HomeController {
	
	@GetMapping
	public ModelAndView home(@ModelAttribute("usuario") Usuario usuario, ModelMap model) {		
		return new ModelAndView("home", model);		
	}
	
	@GetMapping("login")
	public ModelAndView login(@ModelAttribute("usuario") Usuario usuario, ModelMap model) {		
		return new ModelAndView("login", model);		
	}
	
}
