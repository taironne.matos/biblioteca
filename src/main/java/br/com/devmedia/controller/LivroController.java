package br.com.devmedia.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.devmedia.dao.CategoriaDao;
import br.com.devmedia.dao.LivroDao;
import br.com.devmedia.domain.Livro;

@Controller
@RequestMapping("categoria/{idCategoria}/livros")
public class LivroController {

	@Autowired
	private LivroDao dao;
	
	@Autowired
	private CategoriaDao daoCat;
	
	@GetMapping("/todos")
    public ModelAndView listarTodos(@PathVariable("idCategoria") Long idCategoria, ModelMap model) {
        model.addAttribute("livros", dao.recuperarPorCategoria(idCategoria));
        model.addAttribute("idCategoria", idCategoria);
        return new ModelAndView("/book/list", model);
    }
	
	@GetMapping("/cadastro")
    public String cadastro(@ModelAttribute("livro") Livro livro, @PathVariable("idCategoria") Long idCategoria) {
        return "/book/add";
    }
	
	@PostMapping("/save")
    public String salvar(@PathVariable("idCategoria") Long idCategoria, @ModelAttribute("livro") Livro livro, BindingResult result, RedirectAttributes attr) {
		livro.setCategoria(daoCat.getId(idCategoria));
        dao.salvar(livro);
        attr.addFlashAttribute("message", "Livro salvo com sucesso.");
        return "redirect:/categoria/" + idCategoria + "/livros/todos";
    }
	
	@PutMapping("/save")
	public String update(@PathVariable("idCategoria") Long idCategoria, @ModelAttribute("livro") Livro livro, BindingResult result, RedirectAttributes attr) {
		livro.setCategoria(daoCat.getId(idCategoria));
        dao.editar(livro);
        attr.addFlashAttribute("message", "Livro editado com sucesso.");
        return "redirect:/categoria/" + idCategoria + "/livros/todos";
	}
	
	@GetMapping("/{id}/update")
    public ModelAndView preUpdate(@PathVariable("idCategoria") Long idCategoria, @PathVariable("id") Long id, ModelMap model) {
        Livro livro = dao.recuperarPorCategoriaIdELivroId(idCategoria, id);
        livro.setId(id);
        model.addAttribute("livro", livro);
        model.addAttribute("idCategoria", idCategoria);
        return new ModelAndView("/book/add", model);
    }
	
	@GetMapping("/{id}/delete")
    public String delete(@PathVariable("idCategoria") Long idCategoria, @PathVariable("id") Long id, RedirectAttributes attr) {		
		dao.excluir(dao.recuperarPorCategoriaIdELivroId(idCategoria, id).getId());
        attr.addFlashAttribute("message", "Livro excluído com sucesso.");
        return "redirect:/categoria/" + idCategoria + "/livros/todos";
    }
	
}
