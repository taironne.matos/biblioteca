package br.com.devmedia.dao;

import java.util.List;

import br.com.devmedia.domain.Categoria;

public interface CategoriaDao {
	
	void salvar (Categoria categoria);
	void editar (Categoria categoria);
	void excluir (Long id);
	Categoria getId(Long id);
	List<Categoria> getTodos();

}
