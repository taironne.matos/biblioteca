package br.com.devmedia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.devmedia.domain.Categoria;



@Repository
public class CategoriaDaoImpl implements CategoriaDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional
	@Override
	public void salvar(Categoria categoria) {
		entityManager.persist(categoria);		
	}

	@Transactional
	@Override
	public void editar(Categoria categoria) {
		entityManager.merge(categoria);                      
	}

	@Transactional
	@Override
	public void excluir(Long id) {
		entityManager.remove(entityManager.getReference(Categoria.class, id));                            
	}

	@Transactional(readOnly = true)
	@Override
	public Categoria getId(Long id) {
		String jpql = "from Categoria c where c.id = :id";
		TypedQuery<Categoria> query = entityManager.createQuery(jpql, Categoria.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}

	@Transactional(readOnly = true)
	@Override
	public List<Categoria> getTodos() {
		String jpql = "from Categoria c";
		TypedQuery<Categoria> query = entityManager.createQuery(jpql, Categoria.class);
		return query.getResultList();
	}

}
