package br.com.devmedia.dao;

import java.util.List;

import br.com.devmedia.domain.Livro;

public interface LivroDao {
	
	void salvar(Livro livro);
    List<Livro> recuperarPorCategoria(Long idCategoria); 
    Livro recuperarPorCategoriaIdELivroId(Long idCategoria, Long id);
    void editar(Livro livro);
    void excluir(Long id);
	
}
