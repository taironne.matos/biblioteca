package br.com.devmedia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.devmedia.domain.Livro;

@Repository
public class LivroDaoImpl implements LivroDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional
	@Override
	public void salvar(Livro livro) {
		entityManager.persist(livro);		
	}

	@Override
	public List<Livro> recuperarPorCategoria(Long idCategoria) {
		return entityManager.createQuery("select l from Livro l where l.categoria.id = :idCategoria", Livro.class)
                .setParameter("idCategoria", idCategoria)
                .getResultList();
	}

	@Transactional
	@Override
	public void editar(Livro livro) {
		entityManager.merge(livro);                      
	}

	@Transactional
	@Override
	public void excluir(Long id) {
		entityManager.remove(entityManager.getReference(Livro.class, id));                            
	}

	@Override
	public Livro recuperarPorCategoriaIdELivroId(Long idCategoria, Long id) {
		return entityManager.createQuery("select l from Livro l where l.categoria.id = :idCategoria and l.id = :id", Livro.class)
                .setParameter("idCategoria", idCategoria)
                .setParameter("id", id)
                .getSingleResult();
	}

}
