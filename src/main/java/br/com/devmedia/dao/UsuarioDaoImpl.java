package br.com.devmedia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.devmedia.domain.Usuario;

@Repository
public class UsuarioDaoImpl implements UsuarioDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional
	@Override
	public void salvar(Usuario usuario) {
		entityManager.persist(usuario);		
	}

	@Transactional
	@Override
	public void editar(Usuario usuario) {
		entityManager.merge(usuario);                      
	}

	@Transactional
	@Override
	public void excluir(Long id) {
		entityManager.remove(entityManager.getReference(Usuario.class, id));                            
	}

	@Transactional(readOnly = true)
	@Override
	public Usuario getId(Long id) {
		String jpql = "from Usuario u where u.id = :id";
		TypedQuery<Usuario> query = entityManager.createQuery(jpql, Usuario.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}

	@Transactional(readOnly = true)
	@Override
	public List<Usuario> getTodos() {
		String jpql = "from Usuario u";
		TypedQuery<Usuario> query = entityManager.createQuery(jpql, Usuario.class);
		return query.getResultList();
	}	
	
}
