package br.com.devmedia.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.devmedia.dao.CategoriaDao;
import br.com.devmedia.domain.Categoria;

@RestController
@RequestMapping(value="/api/categorias", 
				produces=MediaType.APPLICATION_JSON_UTF8_VALUE, 
				consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
public class CategoriaResource {
	
	@Autowired
	private CategoriaDao dao;

	@GetMapping()
	@ResponseStatus(HttpStatus.OK)
	public List<Categoria> getTodos() {
		return dao.getTodos();
	}
	
	@PostMapping()
	public Categoria save(@RequestBody Categoria categoria) {
		dao.salvar(categoria);
		return categoria;
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") Long id) {		
		dao.excluir(id);		
	}
	
	@PutMapping("/{id}")
	public Categoria update(@PathVariable Long id, @RequestBody Categoria categoria) {		
		categoria.setId(id);
		dao.editar(categoria);
		return categoria;
	}
}
