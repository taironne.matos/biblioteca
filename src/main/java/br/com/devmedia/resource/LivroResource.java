package br.com.devmedia.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.devmedia.dao.CategoriaDao;
import br.com.devmedia.dao.LivroDao;
import br.com.devmedia.domain.Categoria;
import br.com.devmedia.domain.Livro;

@RestController
@RequestMapping(value="/api/livros", 
				produces=MediaType.APPLICATION_JSON_UTF8_VALUE, 
				consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
public class LivroResource {
	
	@Autowired
	private LivroDao dao;
	
	@Autowired
	private CategoriaDao daoCat;
	
	@GetMapping("/{idCategoria}")
	@ResponseStatus(HttpStatus.OK)
	public List<Livro> getTodos(@PathVariable("idCategoria") Long idCategoria) {
		return dao.recuperarPorCategoria(idCategoria);
	}
	
	@PostMapping("/{idCategoria}")
	public Livro save(@PathVariable("idCategoria") Long idCategoria, @RequestBody Livro livro) {
		Categoria categoria = daoCat.getId(idCategoria);
		livro.setCategoria(categoria);
		dao.salvar(livro);
		return livro;
	}
	
	@DeleteMapping("/{idCategoria}/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("idCategoria") Long idCategoria, @PathVariable("id") Long id) {		
		dao.excluir(dao.recuperarPorCategoriaIdELivroId(idCategoria, id).getId());		
	}
	
	@PutMapping("/{idCategoria}/{id}")
	public Livro update(@PathVariable("idCategoria") Long idCategoria, @PathVariable("id") Long id, @RequestBody Livro livro) {		
		Categoria categoria = daoCat.getId(idCategoria);
		livro.setId(id);
		livro.setCategoria(categoria);
		dao.editar(livro);
		return livro;
	}

}
