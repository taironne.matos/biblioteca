package br.com.devmedia.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.devmedia.dao.UsuarioDao;
import br.com.devmedia.domain.Usuario;

@RestController
@RequestMapping(value="/api/usuarios", 
				produces=MediaType.APPLICATION_JSON_UTF8_VALUE, 
				consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
public class UsuarioResource {
	
	@Autowired
	private UsuarioDao dao;
	
	@GetMapping()
	@ResponseStatus(HttpStatus.OK)
	public List<Usuario> getTodos() {
		return dao.getTodos();
	}
	
	@PostMapping()
	public Usuario save(@RequestBody Usuario usuario) {
		dao.salvar(usuario);
		return usuario;
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") Long id) {		
		dao.excluir(id);		
	}
	
	@PutMapping("/{id}")
	public Usuario update(@PathVariable Long id, @RequestBody Usuario usuario) {		
		usuario.setId(id);
		dao.editar(usuario);
		return usuario;
	}
	
}
